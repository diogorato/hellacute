﻿using UnityEngine;
using System.Collections;

public class SkillTreeSpotLight : MonoBehaviour {

    private float lastTime = 0;

    public float[] blinkThreshold;

    public float currentThreshold;

	// Use this for initialization
	void Start () {

        currentThreshold = blinkThreshold[Random.Range(0, blinkThreshold.Length)];
    }
	
	// Update is called once per frame
	void Update () {

        lastTime += Time.deltaTime;
        
        if (lastTime > currentThreshold) {
        
            GetComponent<Animator>().SetTrigger("Blink");
            lastTime = 0;
            currentThreshold = blinkThreshold[Random.Range(0, blinkThreshold.Length)];
        }
	}
}
