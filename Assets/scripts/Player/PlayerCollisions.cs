﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerCollisions : MonoBehaviour {

    private bool zoomOut, zoomedOut, cancount;

    private float timestamp;


    // Use this for initialization
    void Start () {
        zoomedOut = false;
        zoomOut = false;
    }

    // Update is called once per frame


    void Update () {
        //camera zoom in/out

        if (zoomOut == true && zoomedOut == false)
        {
            GameObject.Find("Camera").gameObject.GetComponent<Camera>().orthographicSize += 4 * Time.deltaTime;

            if (GameObject.Find("Camera").gameObject.GetComponent<Camera>().orthographicSize >= 10)
            {
                zoomOut = false;
                zoomedOut = true;
            }
        }

        if (zoomOut == false && zoomedOut == true)
        {
            GameObject.Find("Camera").gameObject.GetComponent<Camera>().orthographicSize -= 4 * Time.deltaTime;

            if (GameObject.Find("Camera").gameObject.GetComponent<Camera>().orthographicSize <= 7)
            {
                zoomedOut = false;
            }
        }
    }




    private void OnTriggerStay2D(Collider2D col)
    {

        //CAMERA ZOOM
        if (col.gameObject.tag == "CameraFar")
        {
            zoomOut = true;
        }


        //ALIEN INTERACTION
        if (col.gameObject.tag == "AlienArea"){

            GameObject parent = col.gameObject.transform.parent.gameObject;

            if(parent.transform.FindChild("AlienSpaceship") != null) {
            GameObject alien = parent.transform.FindChild("AlienSpaceship").gameObject;
           
                if (cancount == true) //PUTS THE TIME THAT THE PLAYER ENTER THE TRIGGER AND DOESNT LET THAT VARIABLE CHANGE
                {
                    timestamp = Time.time;
                    cancount = false;
                }
                if (Time.time >= timestamp) //FIRES EVERY 2 SECONDS
                {
                   alien.GetComponent<EnemyShot>().Attack();
                    timestamp = Time.time + 2;
                }
            }
        }

    }



    private void OnTriggerExit2D(Collider2D col)
    {
        //CAMERA UNZOOM
        if (col.gameObject.tag == "CameraFar")
        {
            zoomOut = false;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "mushroom")
        {
            this.GetComponent<charmovement>().blockjump = true;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 25.0f);
            col.gameObject.GetComponent<Animator>().Play("ShroomJump");
        }
    }

    }
