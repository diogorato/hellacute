﻿using UnityEngine;
using System.Collections;


public class weaponcontroll : MonoBehaviour {

    private Vector2 direction;
    private Vector2 mousepos;
    private float angle;
    public Camera cam;

    private float time;
    public float attacktime;

    private bool attacking;

    private bool animated = false;
    private bool animated1 = false;

    public GameObject shield;
    public GameObject sword;

    [SerializeField]
    private float shieldsmall;
    [SerializeField]
    private float shieldnormal;

    public int attackPower;



    void Start () {
        shield.SetActive(false);
        sword.SetActive(false);
        attacking = false;
        attacktime = 0.3f;
        attackPower = 1;
	}
	
	
	void Update () {

        
        //ATTACK
        if (Input.GetButtonDown("Attack") && shield.activeSelf == false)
        {
            attacking = true;
            sword.SetActive(true);
            time = attacktime;

            this.GetComponentInChildren<Animator>().Play("attack");
        }
        if (attacking == true)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
            }
            else
            {

                attacking = false;
                sword.SetActive(false);
            }
        }


        if ((Input.GetButton("ShieldDown") || Input.GetButton("Shield") && attacking == false))
        {

            if (animated == false)
            {
                this.GetComponentInChildren<Animator>().Play("WeaponToShield");
                animated1 = false;
                animated = true;
            }
        }
        if ((Input.GetButtonUp("ShieldDown") || Input.GetButtonUp("Shield")) &&
            !(Input.GetButton("ShieldDown") || Input.GetButton("Shield")))
        {       if (animated1 == false)
            {
                this.GetComponentInChildren<Animator>().Play("ShieldToWeapon");
                animated1 = true;
                animated = false;
            }
        }


            //SHIELD AND SPRITE POSITIONING
            if (((Input.GetAxis("ArrowsX") != 0) || Input.GetAxis("ArrowsY") != 0 || Input.GetButton("ShieldDown") || Input.GetButton("Shield") || Input.GetButton("ShieldUp")) 
        && attacking == false)
            {
            shield.SetActive(true);
            
            
            //roda o escudo consoante as teclas primidas
            if (Input.GetButton("Shield"))
            {
                if (GetComponentInParent<charmovement>().mysprite.flipX == false)
                {
                    transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
                    this.GetComponentInChildren<SpriteRenderer>().flipY = false;
                }
                else
                {
                    transform.rotation = Quaternion.AngleAxis(-180, Vector3.forward);
                    this.GetComponentInChildren<SpriteRenderer>().flipY = true;
                }
            }
            if (Input.GetButton("ShieldDown"))
            {
                transform.rotation = Quaternion.AngleAxis(-90, Vector3.forward);
                this.GetComponentInParent<charmovement>().ShieldDown = true;

            } else this.GetComponentInParent<charmovement>().ShieldDown = false;

        }
        else                                                                    //coloca as sprites viradas para onde a persongaem esta virada
        {
            shield.SetActive(false);

            if (GetComponentInParent<charmovement>().mysprite.flipX == false) 
            {
                transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
                this.GetComponentInChildren<SpriteRenderer>().flipY = false;
            }
            else
            {
                transform.rotation = Quaternion.AngleAxis(-180, Vector3.forward);
                this.GetComponentInChildren<SpriteRenderer>().flipY = true;
            }
        }





        }
}
