﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkillTree : MonoBehaviour {
   public GameObject player;
   public GameObject weapon;

   public int Attack1, Attack2, Attack3, Attack4, Attack5;
   public int Defense1, Defense2, Defense3, Defense4, Defense5;
   public int Agility1, Agility2, Agility3, Agility4, Agility5;


    // Use this for initialization

    void Start () {
        Attack1 = 0;
        Attack2 = 0;
        Attack3 = 0;
        Attack4 = 0;
        Attack5 = 0;
        Defense1 = 0;
        Defense2 = 0;
        Defense3 = 0;
        Defense4 = 0;
        Defense5 = 0;
        Agility1 = 0;
        Agility2 = 0;
        Agility3 = 0;
        Agility4 = 0;
        Agility5 = 0;
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void IncreaseAttack()
    {
        if (Attack1 < 5)
        {
            Attack1 += 1;
            weapon.GetComponent<weaponcontroll>().attackPower += 1;
            this.transform.Find("attack/attack1/button/Text").gameObject.GetComponent<Text>().text = Attack1 + " / 5";
        }
        
    }

    public void IncreaseSpeed()
    {
        if (Agility1 < 5)
        {
            Agility1 += 1;
            player.GetComponent<charmovement>().speed += 1;
            this.transform.Find("agility/agility1/button/Text").gameObject.GetComponent<Text>().text = Agility1 + " / 5";
        }
    }

    public void IncreaseJump()
    {   if (Agility2 < 3)
        {
            Agility2 += 1;
            player.GetComponent<charmovement>().jumpforce += 1;
            this.transform.Find("agility/agility2/button/Text").gameObject.GetComponent<Text>().text = Agility2 + " / 3";
        }
    }

    public void UnlockSlowTime()
    {
        if (Agility3 < 1)
        {
            Agility3 += 1;
            player.GetComponent<charmovement>().unlockslow = true;
            this.transform.Find("agility/agility3/button/Text").gameObject.GetComponent<Text>().text =  "1 / 1";
        }
    }

}
