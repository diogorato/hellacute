﻿using UnityEngine;
using System.Collections;

public class charmovement : MonoBehaviour {

    private Rigidbody2D mybody;
    public SpriteRenderer mysprite;

    public float speed;



    public float jumpforce;

    public bool unlockslow;

    private int jumpCount, bounceCount;

    public bool blockjump;

    public bool ShieldDown;

 [SerializeField]
    private float slowFactor;

    public Animator anim;

    private bool SlowDownTriggerInUse = false;

    void Start () {
        mybody = this.GetComponent<Rigidbody2D>();
        unlockslow = false;
        blockjump = false;
        ShieldDown = false;
        mysprite = this.GetComponent<SpriteRenderer>();
        jumpCount = 0;
    }
	


	void Update () {
        
        float horizontal = Input.GetAxis("Horizontal") + Input.GetAxis("joyX");
        
        anim.speed = (speed) * 0.1f;

        //JUMP FUNCTION
        if (CanJump())
        {
            blockjump = false;
        }
        if (blockjump == false)
        {

            if (!CanJump() && jumpCount == 0)
            {
                jumpCount = 1;
            }


            if (CanJump())
            {   
                jumpCount = 0;
                if (Input.GetButtonDown("JumpW") && jumpCount == 0)
                {
                    mybody.AddForce(new Vector2(0, jumpforce), ForceMode2D.Impulse);
                    jumpCount++;

                }
            }
            else
            {
                if (Input.GetButtonDown("JumpW") && jumpCount == 1)
                {

                    mybody.velocity = new Vector2(mybody.velocity.x, 0);
                    mybody.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
                    jumpCount++;
                }
                else
            if (Input.GetButtonUp("JumpW") && (mybody.velocity.y > 0))
                {
                    mybody.velocity = new Vector2(mybody.velocity.x, 0);
                }

            }

        }


      /*  if (CanBounce() && !CanJump())
        {
            if (Input.GetButtonDown("JumpW"))
            {
                Debug.Log("salteu");
                mybody.AddForce(new Vector2(20, 10), ForceMode2D.Impulse);
            }

        }*/




        if (Input.GetAxis("SlowDownTrigger") != 0 && unlockslow == true)
        {
            Time.timeScale = -Mathf.Lerp(0, slowFactor, Input.GetAxis("SlowDownTrigger")) + 1;
            transform.Find("Attachment/sprites").gameObject.GetComponent<Animator>().speed = 1 + 0.3f + Mathf.Lerp(0, slowFactor, Input.GetAxis("SlowDownTrigger"));
        }

        Movement(horizontal);     
    }



    public bool CanJump ()
    {

 
        Collider2D shieldcol = this.transform.Find("Attachment/shieldtrigger").gameObject.GetComponent<Collider2D>();
        Collider2D feetcol = this.transform.Find("feet").gameObject.GetComponent<Collider2D>();

        if (ShieldDown == true)
        {
         return shieldcol.IsTouchingLayers(LayerMask.GetMask("Platforms")) || feetcol.IsTouchingLayers(LayerMask.GetMask("Platforms"));
        }
        
        return feetcol.IsTouchingLayers(LayerMask.GetMask("Platforms"));
    }

    /*public bool CanBounce()
    {
        Collider2D frontcol = this.transform.Find("front").gameObject.GetComponent<Collider2D>();
        return frontcol.IsTouchingLayers(LayerMask.GetMask("Platforms"));
    }*/


    private void Movement (float horizontal)
    {
        if((horizontal < 0.4f) && horizontal > 0) { horizontal = 0; }
        if ((horizontal > -0.4f) && horizontal < 0) { horizontal = 0; }
        //ANIMATION
        if (horizontal != 0)
        {
            anim.Play("walk");
        }
        else
        {
            anim.Play("iddle");
        }


        //FLIP SPRITE
        if(horizontal < 0)
        {
            mysprite.flipX = true;
        }
        if(horizontal > 0)
        {
            mysprite.flipX = false;
        }



        //SPRINT
        if (Input.GetButtonDown("ShieldDown"))
        {
            speed += 5;

        }
        if (Input.GetButtonUp("ShieldDown"))
        {
            speed -= 5;
        }
        mybody.velocity = new Vector2(horizontal * (speed), mybody.velocity.y); 
        
    }






}
