﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformGenerationScript : MonoBehaviour {

    public int levelWidth;
    
    public GameObject[] platformsTemplate;

    private List<GameObject> platforms;

    private Vector3 getBeginPointPosition(GameObject platform) {

        return platform.transform.FindChild("BeginSnap").position;

    }

    private Vector3 getEndPointPosition(GameObject platform){

        return platform.transform.FindChild("EndSnap").position;
    }

    /// <summary>
    /// Select the method for creating the level
    /// </summary>
    public void run() {

        //Initialize Variables
        platforms = new List< GameObject>();

        randomGeneration();

    }	

    private void randomGeneration() {

        Vector3 generationPosition = GetComponent<Transform>().position;

        while ( (getNextAvailablePoint() - generationPosition).x < levelWidth) {

            //Choose Platform Randomly
            int platformId = Random.Range(0, platformsTemplate.Length);

            //Get Point to Snap new Platform
            Vector3 snapPoint = getNextAvailablePoint();

            //Create new Platform
            GameObject obj = (GameObject)Instantiate(platformsTemplate[platformId], new Vector3(0,0,0), Quaternion.identity);

            //Snap begin point of new platform to the snapPoint
            translatePlatform(obj, snapPoint);

            //Add platform to list
            platforms.Add(obj);


        }

    }

    private Vector3 getNextAvailablePoint() {
    
        //If level is empty
        if(platforms.Count == 0) {

            return GetComponent<Transform>().position;

        } else {

            return getEndPointPosition(platforms[platforms.Count - 1]);

        }

    }

    private void translatePlatform(GameObject obj, Vector3 snapPoint) {

        Vector3 pointToSnap = getBeginPointPosition(obj);

        Vector3 translation = snapPoint - pointToSnap;

        obj.transform.Translate(translation);

    }
}
