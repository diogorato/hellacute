﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

    public Transform canvas;


	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("SkillTree"))
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                canvas.gameObject.SetActive(true);
                Time.timeScale = 0;
                this.gameObject.GetComponent<charmovement>().enabled = false;
            }
            else
            {
                canvas.gameObject.SetActive(false);
                this.gameObject.GetComponent<charmovement>().enabled = true;

                Time.timeScale = 1;
            }
        }
    }
}
