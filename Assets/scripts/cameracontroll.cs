﻿using UnityEngine;
using System.Collections;

public class cameracontroll : MonoBehaviour {
    private float playerposx, playerposy;
    public Transform Player;

    public Vector2
           margin,
           smoothing;

    public bool isFollowing;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        var x = transform.position.x;
        var y = transform.position.y;

        if (isFollowing)
        {
            if (Mathf.Abs(x - Player.position.x) > margin.x)
                x = Mathf.Lerp(x, Player.position.x, smoothing.x * Time.deltaTime);

            if (Mathf.Abs(y - Player.position.y + 3) > margin.y+3)
                y = Mathf.Lerp(y, Player.position.y + 3, smoothing.y+3 * Time.deltaTime);
        }


        if (GameObject.Find("player") == null)
        {
            FindPlayer();
            return;
        }

        this.transform.position = new Vector3(x, y, -10); ;
    }

    float nextTimeToSearch = 0;
    void FindPlayer()
    {
        if (nextTimeToSearch <= Time.time)
        {
            GameObject searchResult = GameObject.FindGameObjectWithTag("Player");
            if (searchResult != null)
                Player = searchResult.transform;
            nextTimeToSearch = Time.time + 0.5f;
        }
    }
}
