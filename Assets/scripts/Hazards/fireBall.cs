﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class fireBall : MonoBehaviour {
	Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = this.gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "destroyer") {
			rb.velocity = new Vector2(0.0f, 31.0f);
		}

		if (col.collider.name == "shieldtrigger") {
			GameObject.Find ("player").gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 0, 0);
			GameObject.Find("player").gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0.0f, 25.0f), ForceMode2D.Impulse);
		}
		if (col.collider.name == "player") {
            GameObject.Find("GameManager").gameObject.GetComponent<GameManager>().Death();
        }
	}
}
