﻿using UnityEngine;
using System.Collections;

public class icecreamTrigger : MonoBehaviour
{
    Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {
        rb = this.GetComponentInParent<Rigidbody2D>();
        rb.gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player"){
            rb.isKinematic = false;
            rb.gravityScale = 4.2f;
        }
    }
}