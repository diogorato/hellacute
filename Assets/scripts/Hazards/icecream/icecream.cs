﻿using UnityEngine;
using System.Collections;

public class icecream : MonoBehaviour
{
    Rigidbody2D rb;
    public Sprite spritefall;

    // Use this for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D> ();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D col){
        if(col.gameObject.tag == "platform"){
            rb.isKinematic = true;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = spritefall;
        }
        if(col.gameObject.tag == "mushroom" || col.gameObject.tag == "destroyer")
        {
            Object.Destroy(this.gameObject);
        }
    }
}
