﻿using UnityEngine;
using System.Collections;

public class step : MonoBehaviour {

    private bool touchedStep;
    private float timer;
    public float fallspeed;

	// Use this for initialization
	void Start () {
        touchedStep = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (touchedStep == true)
        {
            timer += Time.deltaTime;
            if (timer > fallspeed)
            {
                this.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
                this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Player")
        {
            touchedStep = true;
        }

        if (col.gameObject.tag == "destroyer")
        {
            Object.Destroy(this.gameObject);
        }
    }
}
