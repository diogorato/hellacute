﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public int LastScenePlayed;
    private int SkillTreeScene = 1;

    public int playerLives;
    private GameObject player;

    private Dictionary<string, float> playerstats;
    public Dictionary<string, float> PlayerStats{

        get { 
            return playerstats; 
        }

        set { 
            playerstats = value;
            UpdatePlayerStats();
        }
    }

	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(this);
        player = GameObject.FindGameObjectWithTag("Player");

    }
	
	// Update is called once per frame
	void Update () {

	    if (!player){

            player = GameObject.FindGameObjectWithTag("Player");
        
        }
	}

    public void SucessfulLevelEnd() {

        LastScenePlayed = SceneManager.GetActiveScene().buildIndex;
        LoadSkillTree();
        
    }

    public void ResumeLevelSequence() {

        if (LastScenePlayed == 0)
            LastScenePlayed++;
        SceneManager.LoadScene(LastScenePlayed+1);

    }

    public void TryAgainCurrentScene() {

        SceneManager.LoadScene(LastScenePlayed + 1);

    }

    public void Death(){
    
        LastScenePlayed = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(LastScenePlayed);
    
    }

    public void LoadSkillTree(){

        SceneManager.LoadScene(SkillTreeScene);

    }

    private void UpdatePlayerStats() {

        foreach(KeyValuePair<string, float> statPair in PlayerStats) {
            //player.UpdateStat(statPair.Key, statPair.Value);
        }

    }

}
