﻿using UnityEngine;
using System.Collections;

public class BearBehaviour : MonoBehaviour {

    private bool isFrooze;
    private GameObject player;
	
    // Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {

    }

    //ENEMY STUNS PLAYER
    private void OnCollisionStay2D(Collision2D coll)
    {   
        if (coll.collider.name == "player")
        {

          //  player = coll.gameObject;
            GameObject.Find("GameManager").gameObject.GetComponent<GameManager>().Death();

            //StartCoroutine(freeze(coll));
        }
    }



   /* private IEnumerator freeze(Collision2D collision)
    {   
        collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        isFrooze = true;
        yield return new WaitForSeconds(3);
        collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

    }*/


    //ENEMY FREES THE PLAYER WHEN DEAD
     void OnTriggerEnter2D (Collider2D coll)
    {
        if (coll.gameObject.tag == "attack")
        {
            if (this.gameObject.GetComponent<EnemyControlller>().getHP() <= 0)
            {   
              /*  if(isFrooze == true)
                {
                    player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                    player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
               }*/
                Object.Destroy(this.gameObject);
            }
        }
    }

}
