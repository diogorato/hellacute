﻿using UnityEngine;
using System.Collections;

public class ChasePlayer : MonoBehaviour {

    public GameObject enemy;
    public float speed;

    private bool OutOfRange;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (OutOfRange == true && enemy != null)
        {
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, new Vector3(transform.position.x, enemy.transform.position.y, 0), speed * Time.deltaTime);
        }
	}

    //follows the player on the X axis while he's inside the trigger area;
    void OnTriggerStay2D(Collider2D coll)
    { if(coll.gameObject.tag == "Player")
        {
            if (enemy != null)
            {   
                enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, new Vector3(coll.gameObject.transform.position.x, enemy.transform.position.y, 0), speed * Time.deltaTime);
                OutOfRange = false;
            }
        }
        
    }


    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (enemy != null)
            {
                OutOfRange = true;

            }
        }

    }
}
