﻿using UnityEngine;
using System.Collections;

public class EnemyShot : MonoBehaviour {

    // Projectile prefab for shooting

    public Rigidbody2D shotPrefab;
    public Transform player;
    public int cooldown = 2;
    public float nextShot = 0;
    public float deleteDelay = 5;

    public bool attackflag;

    public float shootingRate = 0.25f;

    void Start()
    {
        attackflag = false;
    }

    void Update()
    {
        if (this.gameObject.GetComponent<EnemyControlller>().getHP() <= 0)
        {
            Object.Destroy(this.gameObject);

        }
    }

    public void Attack() {

            Rigidbody2D clone;
            clone = (Instantiate(shotPrefab, new Vector3(transform.position.x, transform.position.y - 1.0f, 0), transform.rotation) as Rigidbody2D);
            clone.transform.LookAt(GameObject.Find("player").transform.position);
            clone.GetComponent<Rigidbody2D>().AddForce(clone.transform.forward * 250);
            if (Time.time >= deleteDelay) {
                Destroy(clone.gameObject, 3.0f);
                deleteDelay += cooldown;
            }

    }

    
}
