﻿using UnityEngine;
using System.Collections;

public class CannonTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator OnTriggerEnter2D(Collider2D col)
    {
        if( col.gameObject.tag == "Player")
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            this.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
