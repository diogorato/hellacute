﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour {

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.name == "shieldtrigger" || coll.gameObject.tag == "platform" || coll.gameObject.tag == "IceCream" || coll.gameObject.tag == "destroyer") {
            Object.Destroy(this.gameObject);
        }
        if (coll.collider.name == "player" || coll.collider.name == "attacktrigger") {
            GameObject.Find("GameManager").gameObject.GetComponent<GameManager>().Death();
        }
    }
}
