﻿using UnityEngine;
using System.Collections;

public class EnemyControlller : MonoBehaviour {

    public int HP;
    public int XP;
    public int attack;
    private float direction;

	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
        if (HP <= 0)
        {
            Destroy(this.gameObject);
        }
    }


    //WHEN GETS HIT ENEMY IS THROWN BACK, TAKES DAMAGE AND GIVES HP;
    void OnTriggerEnter2D (Collider2D coll)
    {

        if ( coll.gameObject.tag == "attack")
        {

            direction = Mathf.Atan2(coll.gameObject.transform.right.z, coll.gameObject.transform.right.x) * Mathf.Rad2Deg;
  
            direction = -Mathf.Lerp(-1, 1, direction);


            this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(12 * direction, 0), ForceMode2D.Impulse);
            HP -= coll.gameObject.GetComponentInParent<weaponcontroll>().attackPower;

           
            coll.gameObject.SetActive(false);

        }


        if (coll.gameObject.tag == "Explosion")
        {
            HP -= coll.gameObject.GetComponentInParent<CupCake>().ExplosionDamage;
        }

        }

    public int getHP()
    {
        return HP;
    }
}
