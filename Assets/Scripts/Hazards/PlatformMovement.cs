﻿using UnityEngine;
using System.Collections;

public class PlatformMovement : MonoBehaviour {
    public bool x, y;
    private bool end;
    public int distance;
    Vector3 startpos;
    public float speed;
    float time;
   private Rigidbody2D rb;
    private Vector2 goalPos;

    FixedJoint2D joint;

    // Use this for initialization
    void Start () {
        time = Time.time;

        end = false;
        startpos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
       // goalPos = new Vector2((startpos.x + distance) + Time.time * speed, startpos.y);
        joint =  GetComponent<FixedJoint2D>();

        rb = GetComponent<Rigidbody2D>();



    }
    void Update()
    {
        
    }


        // Update is called once per frame
        void FixedUpdate () {



        if (x == true)
        {
            if (transform.position.x < startpos.x + distance && end == false)
            {
                goalPos = (new Vector2(startpos.x + distance , transform.position.y) - new Vector2(transform.position.x, transform.position.y)).normalized;

            }
            else
            {
                end = true;
                if (transform.position.x > startpos.x && end == true)
                {
                    goalPos = (new Vector2(startpos.x-1, transform.position.y) - new Vector2(transform.position.x, transform.position.y)).normalized;
                }
                else end = false;
            }

            rb.MovePosition(rb.position + goalPos * speed * Time.deltaTime);
        }



        if (y == true)
        {
            if (transform.position.y < startpos.y + distance && end == false)
            {
                goalPos = (new Vector2(transform.position.x, startpos.y + distance + 1) - new Vector2(transform.position.x, transform.position.y)).normalized;

            }
            else
            {
                end = true;
                if (transform.position.y > startpos.y && end == true)
                {
                    goalPos = (new Vector2(transform.position.x, startpos.y) - new Vector2(transform.position.x, transform.position.y)).normalized;
                }
                else end = false;
            }
            rb.MovePosition(rb.position + goalPos * speed * Time.deltaTime);
        }




    }
    void ConnectTo(Rigidbody2D character)
    {
       
        joint.connectedBody = character;
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        if ((collision.collider.name == "feet" || collision.collider.name == "shieldtrigger") && (Input.GetAxis("joyX") < 0.4 && Input.GetAxis("joyX") > -0.4))
        {
           ConnectTo(collision.gameObject.GetComponent<Rigidbody2D>());
        }

        if (Input.GetButtonDown("JumpW") || Input.GetButtonDown("ShieldDown") || Input.GetButtonUp("ShieldDown"))
        {
            joint.connectedBody = null;
        }

        if (Input.GetAxis("joyX") > 0.4 || Input.GetAxis("joyX") < -0.4)
        {
            joint.connectedBody = null;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.name == "feet")
        {
            joint.connectedBody = null;
        }
    }

    

}
