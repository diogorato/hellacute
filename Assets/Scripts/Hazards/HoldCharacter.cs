﻿using UnityEngine;
using System.Collections;

public class HoldCharacter : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.GetComponent<Collider2D>().name =="feet")
        {
            col.gameObject.transform.parent.GetComponent<Transform>().parent = gameObject.transform;
        }
    }

    

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.GetComponent<Collider2D>().name == "feet")
        {
            col.gameObject.transform.parent.GetComponent<Transform>().parent = null;
        }
    }
}
