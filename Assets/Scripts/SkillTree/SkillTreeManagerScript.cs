﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillTreeManagerScript : MonoBehaviour {

    public GameObject player;

    public Text PointsAvailableText;

    public uint PointsAvailable;

    public Button[] SkillButtons;

    private bool InSkillTree = false;

    private GameManager GameManager;

	// Use this for initialization
	void Start () {

        GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        GameManager.PlayerStats = GetSkillTreeValues(); 

        if(PointsAvailableText == null) {
            Debug.LogError("Skill Tree Points Available Text not assign");
        }

        UpdatePointsText();

        InSkillTree = false;
        
    }
	
    // Update is called once per frame
	void Update () {

        GameManager.PlayerStats = GetSkillTreeValues();

        //Handle Input for Active Screen
        if (Input.GetButtonDown("SkillTree")) {

            InSkillTree = !InSkillTree;

            if(InSkillTree) {

                DisablePlayer();
                SelectFirstButton();

            } else{

                EnablePlayer();
                DeselectAllButtons();
            }
            
        }
	}

    private void ParseSkillTreePoints(Dictionary<string, float> points) {

        foreach(Button skillButton in SkillButtons) {

            SkillTreeButtonScript script = skillButton.GetComponent<SkillTreeButtonScript>();

            float CurrentPoint = points[script.GetLevelTag()];

            script.SetSkillPoints(CurrentPoint);

        }    

    }

    public Dictionary<string, float> GetSkillTreeValues() {

        Dictionary<string, float> results = new Dictionary<string, float>();

        foreach(Button skillButton in SkillButtons) {

            SkillTreeButtonScript script = skillButton.GetComponent<SkillTreeButtonScript>();

            results.Add(script.GetLevelTag(), script.GetLevelValue());

        }

        return results;
    }

    public void AddPointsAvailable(uint numPoints = 1) {

        PointsAvailable += numPoints;
        UpdatePointsText();

    }

    public void RemovePointsAvailable(uint numPoints = 1) {

        PointsAvailable -= numPoints;
        UpdatePointsText();

    }

    public void AssignPoints(uint numPoints) {

        PointsAvailable = numPoints;
        UpdatePointsText();

    }

    public uint GetNumberPointsAvailable() {
    
        return PointsAvailable;

    }

    private void UpdatePointsText() {

        PointsAvailableText.text = PointsAvailable == 1 ? "1 point available!" : (PointsAvailable + " points available!");        

    }

    private void DisablePlayer() {

        player.GetComponent<charmovement>().enabled = false;

        player.GetComponentInChildren<weaponcontroll>().enabled = false;
    
    }

    private void EnablePlayer() {

        player.GetComponent<charmovement>().enabled = true;

        player.GetComponentInChildren<weaponcontroll>().enabled = true;

    }

    private void SelectFirstButton() {

        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(SkillButtons[0].gameObject);

    }

    private void DeselectAllButtons() {

        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);

    }
}
