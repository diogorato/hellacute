﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SkillTreeButtonScript : MonoBehaviour, ISelectHandler {

    //Tips Variables
    public string Tip = "";

    public Text TipTextBox;

    //Level Meta Data
    public Button PreviousLevel;

    private SkillTreeButtonScript PreviousButtonScript;

    public string LevelTag = "";

    private const int MAXLEVEL = 3;

    public int Level = 0;

    public float[] LevelValues;

    //Sprites for the levels
    public Sprite[] spriteLevel;

    //Skill Tree Manager
    private SkillTreeManagerScript skillTreeManager;

	// Use this for initialization
	void Start () {

        skillTreeManager = GameObject.Find("Skill Tree Manager").GetComponent<SkillTreeManagerScript>();

        if (skillTreeManager == null) {
            Debug.LogError("Impossible to assign Skill Tree Manager Script inside Skill Tree Button Script");
        }

        if (LevelValues.Length != MAXLEVEL + 1) {
            Debug.LogError("Wrong Values Inside Skill Tree: Level Values does not match the number of levels");
        }

        if (spriteLevel.Length != MAXLEVEL + 1) {
            Debug.LogError("Wrong Values Inside Skill Tree: Sprites number does not match the number of levels");
        }

        if(PreviousLevel != null) { 
            PreviousButtonScript = PreviousLevel.GetComponent<SkillTreeButtonScript>();
        }

        changeSkullSprite(Level);

    }

    public void OnSelect(BaseEventData eventData) {

        TipTextBox.text = Tip;

    }

	// Update is called once per frame
	void Update () {

        if (PreviousLevel != null) {
            
            if(PreviousButtonScript.IsFull()) {
                GetComponent<Button>().interactable = true;
            } else {
                GetComponent<Button>().interactable = false;
                GetComponent<Animator>().SetTrigger("Disabled");
            }
                
        }

    }

    public void UpgradeLevel() {

        //Has Available Points and Still has levels to upgrade
        if (skillTreeManager.GetNumberPointsAvailable() > 0 && Level < MAXLEVEL) {

            //Decrease Points Used
            skillTreeManager.RemovePointsAvailable();

            //Increase Level
            Level++;

            //Change Skull
            changeSkullSprite(Level);

        }

    }

    private void changeSkullSprite(int level) {
    
        switch(level) {
            case 0:
                this.transform.GetChild(0).GetComponent<Image>().sprite = spriteLevel[0];
                break;
            case 1:
                this.transform.GetChild(0).GetComponent<Image>().sprite = spriteLevel[1];
                break;
            case 2:
                this.transform.GetChild(0).GetComponent<Image>().sprite = spriteLevel[2];
                break;
            case 3:
                this.transform.GetChild(0).GetComponent<Image>().sprite = spriteLevel[3];
                break;
        }

    }

    public string GetLevelTag() {

        return LevelTag;

    }

    public float GetLevelValue() {

        return LevelValues[Level];

    }

    public bool IsFull() {

        return Level == MAXLEVEL;

    }

    public void SetSkillPoints(float value) {

        for (int i = 0; i < LevelValues.Length; i++) {

            if (value == LevelValues[i]) {
                Level = i;
                break;
            }

        }

        changeSkullSprite(Level);
    }
}
