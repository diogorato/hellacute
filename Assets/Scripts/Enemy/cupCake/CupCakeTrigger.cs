﻿using UnityEngine;
using System.Collections;

public class CupCakeTrigger : MonoBehaviour
{

    private Vector3 startpos;
    private bool triggered;


    // Use this for initialization
    void Start()
    {
        triggered = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {   if (triggered == true)
            {
                startpos = this.transform.position;
                triggered = false;
                StartCoroutine(SelfDestruct());
            }
            
        }
    }


    IEnumerator SelfDestruct()
    {

        this.GetComponentInParent<CupCake>().stopmovement = true;
        //ANIMAÇÂO DE QUASE REBENTAR
        yield return new WaitForSeconds(1.5f);
        transform.GetChild(0).gameObject.SetActive(true);
        //ANIMAÇÂO DE EXPLODIR
        yield return new WaitForSeconds(0.5f);
        Destroy(transform.parent.gameObject);
    }
}
