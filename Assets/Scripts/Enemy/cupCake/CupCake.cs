﻿using UnityEngine;
using System.Collections;

public class CupCake : MonoBehaviour
{

    public float distance;
    public float speed;

    private float counter;

    private bool end;
    private bool hit;
    private Vector2 goalPos;
    private Rigidbody2D rb;

    public int ExplosionDamage;

    public bool stopmovement;

    private Vector3 startpos;
    // Use this for initialization
    void Start()
    {
        hit = false;
        stopmovement = false;
        startpos = transform.position;
        rb = this.gameObject.GetComponent<Rigidbody2D>();

        ExplosionDamage = 10;
    }

    // Update is called once per frame
    void Update()
    {


        if (hit == false && stopmovement == false)
        {
            if (transform.position.x < startpos.x + distance && end == false)
            {
                goalPos = (new Vector2(startpos.x + distance, transform.position.y) - new Vector2(transform.position.x, transform.position.y)).normalized;

            }
            else
            {
                end = true;
                if (transform.position.x > startpos.x && end == true)
                {
                    goalPos = (new Vector2(startpos.x - 1, transform.position.y) - new Vector2(transform.position.x, transform.position.y)).normalized;
                }
                else end = false;
            }

            rb.MovePosition(rb.position + goalPos * speed * Time.deltaTime);
        }

        //transform.position = new Vector3 (Mathf.PingPong(Time.time *speed, distance)  + startpos.x , this.transform.position.y, this.transform.position.z);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "attack")
        {
            StartCoroutine(Attacked());   
        }
    }

    IEnumerator Attacked()
    {
        hit = true;
        yield return new WaitForSeconds(1);
        hit = false;
    }
}