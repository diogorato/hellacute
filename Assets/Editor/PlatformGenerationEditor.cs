﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlatformGenerationScript))]
public class PlatformGenerationEditor : Editor {

    public override void OnInspectorGUI(){

        DrawDefaultInspector();

        PlatformGenerationScript script = (PlatformGenerationScript)target;

        if(GUILayout.Button("Generate Platforms")) {

            script.run();
        }


    }

}
